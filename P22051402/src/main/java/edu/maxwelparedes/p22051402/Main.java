package edu.maxwelparedes.p22051402;

/**
 *
 * @author Maxwel Paredes <maxwel.paredes.l@uni.pe>
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Exception 2 ...");
    
        try{
            myExceptionFunction();
            Function();
        }catch(ArithmeticException e){
            System.out.println("Arithmetic exception: "+e);
        }catch(MyException e){
            System.out.println("MyException: "+e);
        }catch(Exception e ){
            System.out.println("Exception: "+e);
        }
        
        System.out.println("End ...");
    }
    
    public static void Function () throws ArithmeticException{
        int i = 1/0;
    }
    public static void myExceptionFunction() throws MyException {
        throw new MyException("My error !!!");
    }
}
