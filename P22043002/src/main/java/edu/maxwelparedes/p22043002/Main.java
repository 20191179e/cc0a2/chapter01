/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.maxwelparedes.p22043002;

import static java.lang.Math.E;
import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.log;
import static java.lang.Math.pow;
import static java.lang.Math.round;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

/**
 *
 * @author Maxwel Paredes <maxwel.paredes.l@uni.pe>
 */

public class Main {
    public static void main(String[] args) {
        System.out.println("Math functions");
        
        float i = 10.25f;
        // round
        System.out.println("i:"+round(i));
        // log
        System.out.println(E);
        System.out.println("ln(e) = "+log(E));
        // power
        System.out.println("2^8 = "+pow(2,8));
        // sqrt
        System.out.println("256^0.5 = "+sqrt(256));
        // trigonometric functions
        System.out.println("sen(0): "+sin(0));
        System.out.println("sen(PI/2): "+ sin(PI/2));
        System.out.println("cos(PI): "+ cos(PI));
    }
}
