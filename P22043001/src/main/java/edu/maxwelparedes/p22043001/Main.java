/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.maxwelparedes.p22043001;

/**
 *
 * @author Maxwel Paredes <maxwel.paredes.l@uni.pe>
 */
public class Main {

    public static void main(String[] args){
        
        System.out.println("Loop statements");
        
    //    int i;
        
        for(int i = 0 ; i<10 ; i++){
            System.out.println(i);
        }
         
        
        {
            int j = 0;
            
            System.out.println(++j);
        }
        //System.out.println(j);
            
        
        for(int i=9; 0<=i ; i--){
            System.out.println(i);
        }
        
        // Matrix 5x3
        
        System.out.println("Matrix 5x3");
        int f = 5;
        int c = 3;
        
        for( int i = 1; i<=f; i++){
            for(int j=1; j<=c;j++){
                System.out.println("("+i+", "+j+")"+"\t");
            }
            System.out.println("");
        }
        
        System.out.println("While loop");
        
        int i = 0;
        while ( i< 10) {
            i++;
            System.out.println(i);
        }
        i = 10;
        while(i>0){
            System.out.println(i);
            i--;
        }
        
        System.out.println("Matriz 5x3 with while loop");
        i = 1;
        
        while(i<=f) {
            int j=1;
            while(j<=c){
                System.out.print("("+i+", "+j+")"+"\t");
                j++;
            }
            System.out.println("");
            i++;
        }
        
        System.out.println("Interation");
        
        i = 0;
        while(i<0){
            System.out.println("Tis while message is not printed");
        }
        
        do{ 
           
            System.out.println("This do while message is printec once");
            
        }while(i<0);
        
        do {
            int j=1;
            do {
                System.out.println("("+i+", "+j+")"+"\t");
                j++;
            }while(j<=c);
            System.out.println("");
            i++;
        }while(i<=f);
        
        //Print prime numbers between 10 and 90
        
        for( int p = 10; p<=90; p++){
            boolean t = true;
            for(int j=2;j<p;j++){
                if(p%j==0){
                    t = false;
                    break;
                }
            }
            if(p == 53) continue;
            if(t){
                System.out.println("The number is prime: "+p);
            }
        }
    }
}