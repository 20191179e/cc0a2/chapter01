package edu.maxwelparedes.p22050701;

import java.util.Arrays;

/**
 *
 * @author Maxwel Paredes <maxwel.paredes.l@uni.pe>
 */
public class Main {
    public static void main(String[] args){
        System.out.println("Functions");
        Functions();
        Functions("Maxwel");
        Functions("Maxwel", 21);
        String[] result = {"Hello",",","Maxwel","you","are","21","years","old","!!!"};
        Functions(result);
        int suma = Functions(1,2,3,4,5,6,7);
        System.out.println("sum: "+ String.valueOf(suma));
        int a = 2;
        int b = 3;
        //cuando se pasan las variables por referencia cambian su valor despues de ejecutar la funcion.
        //cuando se pasan las variables por valor no cambian su valor despues de ejecutar la funcion.
        int sum = Functions(a,b); 
        System.out.println("a: "+String.valueOf(a)+ ", b: "+String.valueOf(b)+", sum:"+ String.valueOf(sum));
        
        
        StringBuilder stringNumber = new StringBuilder();
        stringNumber.append("0");
        System.out.println("StringNumber: "+ stringNumber);
        Functions(stringNumber);
        System.out.println("StringNumber: "+ stringNumber);
        
        stringNumber.setLength(0);
       
        if(authentication("8", stringNumber)){
            System.out.println("Authentication is Ok");
        }else{
            System.out.println("Authentication is failed, error: "+ stringNumber);
        }
        
    }
    //la firma de la función, result type, Function name, Argument's type
    public static void Functions(){
        System.out.println("Hello World!!");
    }
    
    public static void Functions(String name){
        System.out.println("Hello "+name+" !!");
    }
  
    public static void Functions(String name, int age){
          System.out.println("Hello "+name+" you are "+ String.valueOf(age) +" years old!!!");
    }
   
    public static void Functions(String[] args){
        for(String arg: args){
            System.out.print(arg);
        }
        System.out.println("");
    }
    
    
    /**
     * 
     * @param numbers every number in the argument
     * @return sum of numbers
     */
    public static int Functions(int ... numbers){
        
        int suma = 0;
        for(int number: numbers){
            suma += number;
        }
        System.out.println(Arrays.toString(numbers));
        return suma;
    }
    public static void Functions(StringBuilder reference) {
        reference.setLength(0);
        reference.append("1");
    }
    /**
     * Arguments by reference
     * @param credential assett
     * @param error  type of error
     * @return  true if the authentication is ok, otherwise false
     */
    public static boolean authentication(String credential,StringBuilder error){
        boolean out =  false;
        error.setLength(0);
        if(credential.equals("1")){
            out = true;
            error.append("0"); //authentication is ok
        }else {
            error.append("-1"); //otherwise
        }
        return out;
    }   
}
