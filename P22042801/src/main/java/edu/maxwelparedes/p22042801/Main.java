/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.maxwelparedes.p22042801;

/**
 *
 * @author Maxwel Paredes <maxwel.paredes.l@uni.pe>
 */
public class Main {
    public static void main(String[] args){
        System.out.println("Hello World!");
        // cp original.txt copy.txt
        System.out.println("El numero de argumentos es: "+ args.length);
        System.out.println("args 1: "+args[0]);
        System.out.println("args 2: "+args[1]);
        System.out.println("args 3: "+args[2]);
    
        int a = Integer.valueOf(args[0]);
        int b = Integer.valueOf(args[1]);
        int c = Integer.valueOf(args[2]);
        
        System.out.println(a+b);
    }
}
