package edu.maxwelparedes.p22050702;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Maxwel Paredes <maxwel.paredes.l@uni.pe>
 */
public class Main {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        System.out.println("Files");


        String name = "ByteStream.txt";
        File file = new File(name);
        System.out.println("Location of File: "+ file.getAbsolutePath() );
        System.out.println("Writing a file");
 
        FileOutputStream fileOutStream = new FileOutputStream(file);
        fileOutStream.write(82);
        fileOutStream.write(79);
        fileOutStream.write(78);
        fileOutStream.write(65);
        fileOutStream.write(76);
        fileOutStream.write(68);
        fileOutStream.close();
        
        
        System.out.println("Read the File");
        
        FileInputStream fileInputStream = new FileInputStream(file);
        int decimal;
        while((decimal = fileInputStream.read())!=-1){
            System.out.print((char)decimal);
        }
        
        System.out.println("");
        
        name = "CharacterStream.txt";
        
        file = new File(name);
        
        System.out.println("Location of File: "+ file.getAbsolutePath() );
        FileWriter fileWriter = new FileWriter(file);
        
        fileWriter.write(82);
        fileWriter.write(79);
        fileWriter.write(78);
        fileWriter.write(65);
        fileWriter.write(76);
        fileWriter.write(68);
        fileWriter.close();
        
        System.out.println("Read the File");
        
        FileReader fileReader = new FileReader(file);
        
     
        while((decimal = fileReader.read())!=-1){
            System.out.print((char)decimal);
        }
        System.out.println("");
        System.out.println("List of Files");
        String paths[];
        file = new File("./");
        
        paths = file.list();
        
        for (String path: paths){
            System.out.println(path);
        }
        System.out.println("Creatin directories");
        String directory = "/Files/Binaries/Selected";
        
        String fullPath = file.getAbsolutePath() + directory;
        
        file = new File(fullPath);
        
    
        if(file.mkdirs()){
            
            System.out.println("Directories has been created");
            
        }else{
            
            System.out.println("Directories has already created");
            
        }
    }
}
