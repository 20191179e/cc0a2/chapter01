/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.maxwelparedes.p22051403;

/**
 *
 * @author Maxwel Paredes <maxwel.paredes.l@uni.pe>
 */
public class Enemy {
    int life;
    float health;
    
    public int getLife() {
        return life;
    }
    public void setLife(int life){
        this.life = life;
    }
    public float getHealth() {
        return health;
    }
    public void setHealth(float health){
        this.health = health;
        
    }
    public Enemy () {
        life = 10;
        health = 100;
    }
    
    public Enemy(int life, float health) {
        this.life = life;
        this.health = health;
    }
    public void addLife(int life){
        this.life += life;
    }
    public void addHealth(float health){
        this.health += health;
    }
    @Override
    public String toString () {
        return "Enemy{ "+ " life: " +life+", health: "+health+"  }";
    }
    public void bonusLife (){
        life +=5;
    }
}
