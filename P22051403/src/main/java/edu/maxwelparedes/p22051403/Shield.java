package edu.maxwelparedes.p22051403;

/**
 *
 * @author Maxwel Paredes <maxwel.paredes.l@uni.pe>
 */
public interface Shield {
    public void bonusShield();
}
