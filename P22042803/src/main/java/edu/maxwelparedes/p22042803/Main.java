/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.maxwelparedes.p22042803;

/**
 *
 * @author Maxwel Paredes <maxwel.paredes.l@uni.pe>
 */
public class Main {
    
    public static void main (String[] args) {
        
        System.out.println("Conditional statements");
        
      //int i = 35 ;

        /*   
        if ( 30<i){
            System.out.println("the number is greater to 30");
        }
        
         if (i %2 ==1){
            System.out.println("the number is odd");
        }
         
        if (i % 5 == 0){
            System.out.println("the number is multiple of 5");
        }
//        */        
        /*   
        
        if ( 30<i){
            System.out.println("the number is greater to 30");
        }else 
        
         if (i %2 ==1){
            System.out.println("the number is odd");
        }else
         
        if (i % 5 == 0){
            System.out.println("the number is multiple of 5");
        }else{
            System.out.println("The number is other");
        }
//        */
        
  /*
        if ( 30<i){
            System.out.println("the number is greater to 30 and the program ends");
            return;
        }
        
         if (i %2 ==1){
            System.out.println("the number is odd and the program ends");
                    return;
        }
         
        if (i % 5 == 0){
            System.out.println("the number is multiple of 5 and the program ends");
                    return;
        }
//  */        
  
        int i = 11;
        
        switch( i%2 ){
            
            case 0:
        
                System.out.println("the number is even");
            
                break;
        
            case 1:
                
                System.out.println("the number is odd");
                                
                break;
        
            default:
             
                System.out.println("Any number other");
                
                break;
        }

        
           
             
    }

}
